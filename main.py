from selenium import webdriver

# this line direct us to the chrome driver path

# driver = webdriver.chrome('./Drivers/chromedriver.exe')

driver = webdriver.Chrome('C:\\Users\\Administrator\\PycharmProjects\\NewTours_Test_Project\\Drivers\\chromedriver.exe')

driver.get('http://demo.guru99.com/test/newtours/')

driver.find_element_by_xpath("//a[contains(.,'SIGN-ON')]").click()

driver.find_element_by_xpath("//input[@name='userName']").send_keys("Test123")

driver.find_element_by_xpath("//input[@name='password']").send_keys("Test123")

driver.find_element_by_xpath("//input[@name='submit']").click()

home_verification = driver.find_element_by_xpath("//h3[contains(.,'Login Successfully')]").text

if home_verification == "Login Successfully":
    print("User logged in Successfully")
    # capture screenshot
    driver.close()
    assert True

else:
    print("User Login Failed, user should see : "+home_verification)
    driver.close()
    assert False

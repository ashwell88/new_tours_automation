from selenium import webdriver

# this line direct us to the chrome driver path

# driver = webdriver.chrome('./Drivers/chromedriver.exe')
from selenium.webdriver.support.select import Select

driver = webdriver.Chrome('C:\\Users\\Administrator\\PycharmProjects\\NewTours_Test_Project\\Drivers\\chromedriver.exe')

driver.get('http://demo.guru99.com/test/newtours/')

driver.find_element_by_xpath("//a[contains(text(),'REGISTER')]").click()

driver.find_element_by_xpath("//input[@name='firstName']").send_keys("Ashley")

driver.find_element_by_xpath("//input[@name='lastName']").send_keys("Wells")

driver.find_element_by_xpath("//input[@name='phone']").send_keys("0123456789")

driver.find_element_by_xpath("//input[@id='userName']").send_keys("test@test.com")

driver.find_element_by_xpath("//input[@name='address1']").send_keys("123 Test Road")

driver.find_element_by_xpath("//input[@name='city']").send_keys("Cape Town")


driver.find_element_by_xpath("//input[@name='state']").send_keys("Western Cape")

driver.find_element_by_xpath("//input[@name='postalCode']").send_keys("12345")

# Selecting the Country in the Dropdown option

dropdown_yes = driver.find_element_by_name("country")
dropdown = Select(dropdown_yes)

dropdown.select_by_value("SOUTH AFRICA")

driver.find_element_by_xpath("//input[@id='email']").send_keys("test@test.com")

driver.find_element_by_xpath("//input[@name='password']").send_keys("test1234")

driver.find_element_by_xpath("//input[@name='confirmPassword']").send_keys("test1234")

driver.find_element_by_xpath("//input[@name='submit']").click()

registration = driver.find_element_by_xpath("//b[contains(.,'Note: Your user name is test@test.com.')]").text

driver.find_element_by_xpath("//a[contains(text(),'sign-in')]").click()

driver.find_element_by_xpath("//input[@name='userName']").send_keys("test@test.com")

driver.find_element_by_xpath("//input[@name='password']").send_keys("test1234")

driver.find_element_by_xpath("//input[@name='submit']").click()

verify_login = driver.find_element_by_xpath("//h3[contains(.,'Login Successfully')]").text

# Printing test results

if verify_login == "Login Successfully":
    print("User Registered & Login successfully - Test Passed")
    driver.close()
    assert True

else:
    print("User Login Failed - Test Failed : "+verify_login)
    driver.close()
    assert False
